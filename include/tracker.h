#ifndef tracker_h
#define tracker_h

#include <sys/types.h>
#include <sys/param.h>
#include <sys/socket.h> /* constantes, familles... */
#include <netinet/in.h> /* struct sockaddr_in */
#include <arpa/inet.h>  /* prototypes pour les fonctions dans inet(3N) */
#include <netdb.h>      /* struct hostent */
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdint.h>
#include <time.h>
#include <stdbool.h>

#define PORT 3955
#define MAXFILES 10
#define TIMEOUT 90

typedef unsigned int u_int;
typedef unsigned short u_short;
typedef unsigned char u_char;

/* Communication structures */
struct clientRequest
{
    u_int fileHash;
    u_int peerId;
    u_short port;
};

struct trackerAnswer
{
    u_char status;
    u_char nbPeers;
};

struct peerEntry
{
    time_t lastRequest;
    struct in_addr ipaddr;
    u_int peerId;
    u_short port;
};

/* Internal structures */

struct peerListEntry
{
    struct peerEntry pentry;
    struct peerListEntry* next;
};

struct fileListEntry
{
    struct peerListEntry* peers;
    u_int fileHash;
    u_char nbPeers;
};

struct fileList
{
    struct fileListEntry* list[MAXFILES];
    int nbfiles;
};


u_int hash(u_char *str) __attribute__ ((pure));
void fillTracker(const char* dir);
void startTracker() __attribute__ ((noreturn));
void serveRequest (int fd, struct sockaddr_in from);
void addTorrent(char * filename);
const char *getFilenameExtension(const char *filename) __attribute__ ((pure));
struct fileListEntry * searchHash(u_int hashToSearch) __attribute__ ((pure));
u_int file2hash(int fd);
void supprimerAncienClientsEtDoublons(struct fileListEntry* fEntry);
void supprimerAncienClients(struct peerListEntry** listDesClients);
u_char length(struct peerListEntry* listeDesPairs) __attribute__((pure));
bool exists(u_int id, struct peerListEntry* listeDesPairs) __attribute__((pure));
void miseAJourTimestamp(u_int id, struct peerListEntry* listeDesPairs, time_t time);

#endif
