#include "../include/tracker.h"

/* Global Vars*/

struct fileList filesList;

/* Functions */

u_int hash(u_char* str)
{
    u_int hash_ = 0;
    int c;

    while ((c = (int)(*str++)))
        hash_ += (u_int)c;

    return hash_;
}

u_int file2hash(int fd)
{
    int bytes;
    u_char data[1024];
    u_int h = 0;

    while ((bytes = (int)read(fd, data, 1024)) != 0)
        h += hash(data);

    return h;
}


struct fileListEntry* searchHash(u_int hashToSearch)
{

    int i = 0;
    struct fileListEntry* ptr;

    while(i < filesList.nbfiles)
    {

        ptr = filesList.list[i];

        if(ptr->fileHash == hashToSearch)
        {
            /* Hash found ! */
            return ptr;
        }

        /* Not found*/
        i++;
    }

    return NULL;
}

const char* getFilenameExtension(const char* filename)
{
    const char* dot = strrchr(filename, '.');
    if(!dot || dot == filename) return "";
    return dot + 1;
}

void addTorrent(char* filename)
{

    FILE* fp;
    char* line = NULL;
    size_t len = 0;
    struct fileListEntry* fileEntry;
    u_int hash_;

    fp = fopen(filename, "r");
    if (fp == NULL)
        return;

    /* Format :
     * length (Taille du fichier en octet)
     * hashsum (Unsigned long correspondant à la signature du fichier)
     * name (Nom du fichier)
     * piece length (Taille de chacun des morceaux à télécharger)
     * announce (IP du Tracker)
     * */
    if(getline(&line, &len, fp)==-1)
    {
        printf("L'ajout du torrent a foiré");
        exit(EXIT_FAILURE);
    }
    hash_ = (u_int) atol(line); /*skip*/

    if(getline(&line, &len, fp)==-1)
    {
        printf("L'ajout du torrent a foiré");
        exit(EXIT_FAILURE);
    }
    hash_ = (u_int) atol(line);

    free(line);

    fileEntry = malloc(sizeof(struct fileListEntry));
    fileEntry->fileHash = hash_;

    fileEntry->nbPeers = 0;
    fileEntry->peers = NULL;

    filesList.list[filesList.nbfiles] = fileEntry;
    filesList.nbfiles++;

    printf("%x\n",hash_);
    printf("%s added\n", filename);
}

void supprimerAncienClientsEtDoublons(struct fileListEntry* fEntry)
{
    supprimerAncienClients(&fEntry->peers);
}

void supprimerAncienClients(struct peerListEntry** listeDesClients)
{
    struct peerListEntry* ptr1 = *listeDesClients;
    struct peerListEntry* toDel;

    if(ptr1==NULL)
        return;

    while(ptr1 != NULL && ptr1->next != NULL)
    {
        if(difftime(time(NULL), ptr1->next->pentry.lastRequest) > TIMEOUT)
        {
            toDel=ptr1->next;
            ptr1->next=ptr1->next->next;
            free(toDel);
        }
        ptr1=ptr1->next;
    }

    /* Normalement inutile car la tête de la liste devraît être le plus récent*/
    if(difftime(time(NULL), (*listeDesClients)->pentry.lastRequest) > TIMEOUT)
    {
        toDel=*listeDesClients;
        *listeDesClients=(*listeDesClients)->next;
        free(toDel);
    }
}
u_char length(struct peerListEntry* listeDesPairs)
{
    if(listeDesPairs==NULL)
        return 0;
    return (u_char)(1+length(listeDesPairs->next));
}

bool exists(u_int id, struct peerListEntry* listeDesPairs)
{
    if(listeDesPairs==NULL)
        return false;
    if(listeDesPairs->pentry.peerId==id)
        return true;
    return exists(id, listeDesPairs->next);
}

void miseAJourTimestamp(u_int id, struct peerListEntry* listeDesPairs, time_t time_)
{
    if(listeDesPairs==NULL)
        return;
    if(listeDesPairs->pentry.peerId==id)
    {
        listeDesPairs->pentry.lastRequest=time_;
        return;
    }
    miseAJourTimestamp(id, listeDesPairs->next, time_);
}

void serveRequest (int fd, struct sockaddr_in from)
{

    struct clientRequest req;
    struct trackerAnswer resp;
    struct fileListEntry* fEntry;
    struct peerListEntry* e;
    struct peerListEntry* newe;
    u_int i;
    printf("Serving request ...\n");

    /* Parse Hash|peerId|Port + get IP */
    if(read(fd, &req.fileHash, sizeof(req.fileHash)) == -1)
    {
        perror("Echec de read fileHash");
        exit(EXIT_FAILURE);
    }
    if(read(fd, &req.peerId, sizeof(req.peerId)) == -1)
    {
        perror("Echec de read peerId");
        exit(EXIT_FAILURE);
    }
    if(read(fd, &req.port, sizeof(req.port)) == -1)
    {
        perror("Echec de read port");
        exit(EXIT_FAILURE);
    }

    printf("* hash: %x\n", req.fileHash);
    printf("* peerId: %x\n", req.peerId);
    printf("* port: %x\n", req.port);

    /* If hash exists */
    if((fEntry = searchHash(req.fileHash)) != NULL)
    {
        /* Insert peerId|IP|port in hashmap */
        if(exists(req.peerId, fEntry->peers))
        {
            printf("update of timestamp\n");
            miseAJourTimestamp(req.peerId, fEntry->peers, time(NULL));
        }
        else
        {
            printf("addition to the list\n");
            newe = malloc(sizeof(struct peerListEntry));
            newe->pentry.peerId = req.peerId;
            newe->pentry.port = req.port;
            newe->pentry.ipaddr = from.sin_addr;
            newe->pentry.lastRequest=time(NULL);
            newe->next = fEntry->peers;
            fEntry->nbPeers++;
            fEntry->peers = newe;
        }

        supprimerAncienClientsEtDoublons(fEntry);

        fEntry->nbPeers = length(fEntry->peers);

        /* Return success + peerlist */
        resp.status = 0;
        resp.nbPeers = fEntry->nbPeers;
        if(write(fd, &resp.status, sizeof(resp.status))==-1)
        {
            perror("J'arrive pas à satisfaire la requete : ");
            exit(EXIT_FAILURE);
        }
        if(write(fd, &resp.nbPeers, sizeof(resp.nbPeers))==-1)
        {
            perror("J'arrive pas à satisfaire la requete : ");
            exit(EXIT_FAILURE);
        }

        /* For all entry, write infos */
        e = fEntry->peers;
        i = 0;
        while(e != NULL)
        {
            if(write(fd, &(e->pentry.peerId), sizeof(e->pentry.peerId))==-1)
            {
                perror("J'arrive pas à satisfaire la requete : ");
                exit(EXIT_FAILURE);
            }
            if(write(fd, &(e->pentry.ipaddr), sizeof(e->pentry.ipaddr))==-1)
            {
                perror("J'arrive pas à satisfaire la requete : ");
                exit(EXIT_FAILURE);
            }
            if(write(fd, &(e->pentry.port), sizeof(e->pentry.port))==-1)
            {
                perror("J'arrive pas à satisfaire la requete : ");
                exit(EXIT_FAILURE);
            }
            i++;
            e = e->next;
        }

        printf("* SUCCESS: %x peer sent\n", i);
    }
    else
    {
        /* Else return error + empty list */
        printf("* ERROR: hash %x not found\n", req.fileHash);

        resp.status = 1;
        resp.nbPeers = 0;
        if(write(fd, &resp.status, sizeof(resp.status))==-1)
        {
            perror("J'arrive pas à satisfaire la requete : ");
            exit(EXIT_FAILURE);
        }
        if(write(fd, &resp.nbPeers, sizeof(resp.nbPeers))==-1)
        {
            perror("J'arrive pas à satisfaire la requete : ");
            exit(EXIT_FAILURE);
        }
    }

    /* close */
    close(fd);
}

void startTracker()
{
    struct sockaddr_in soc_in;
    int val;
    int ss;  /* socket d'écoute */

    /* socket Internet, de type stream (fiable, bi-directionnel) */
    ss = socket (PF_INET, SOCK_STREAM, 0);

    /* Force la réutilisation de l'adresse si non allouée */
    val = 1;
    setsockopt (ss, SOL_SOCKET, SO_REUSEADDR, &val, sizeof(val));

    /* Nomme le socket: socket inet, port PORT, adresse IP quelconque */
    soc_in.sin_family = AF_INET;
    /*~ soc_in.sin_addr.s_addr = htonl (INADDR_ANY);*/
    soc_in.sin_addr.s_addr = INADDR_ANY;
    soc_in.sin_port = htons (PORT);
    bind (ss, (struct sockaddr*)&soc_in, sizeof(soc_in));

    printf("Tracker started ...\n");
    /* Listen */
    listen (ss, 5);
    while (1)
    {
        struct sockaddr_in from;
        int len;
        int f;

        /* Accept & Serve */
        len = sizeof (from);
        f = accept (ss, (struct sockaddr*)&from, (u_int*)&len);
        serveRequest(f, from);
    }
}

void fillTracker(const char* dir)
{
    char cwd[1024];
    DIR* dp;
    struct dirent* entry;
    struct stat statbuf;

    /* search all beertorrent in dir */
    printf("Filling tracker ...\n");

    if((dp = opendir(dir)) == NULL)
    {
        perror("opendir");
        return;
    }
    if(getcwd(cwd, sizeof(cwd))==NULL)
    {
        perror("Le remplissage du tracker a raté : ");
        exit(EXIT_FAILURE);
    }

    if(chdir(dir)==-1)
    {
        perror("Le remplissage du tracker a raté : ");
        exit(EXIT_FAILURE);
    }

    while((entry = readdir(dp)) != NULL)
    {
        lstat(entry->d_name,&statbuf);
        if(S_ISREG(statbuf.st_mode))
        {
            /* Found a regular file, if ext = .beertorrent, read it */
            if(strcmp(getFilenameExtension(entry->d_name), "beertorrent") == 0)
            {
                addTorrent(entry->d_name);
            }
        }
    }
    closedir(dp);

    if(chdir(cwd)==-1)
    {
        perror("Le remplissage du tracker a raté : ");
        exit(EXIT_FAILURE);
    }
}

int main(int argc, char* argv[])
{
    const char* directory;
    if(argc > 1)
        directory = argv[1];
    else
        directory = "./beertorrent";

    /* Init struct */
    filesList.nbfiles = 0;

    /* Fill tracker */
    fillTracker(directory);

    /* Start routine */
    startTracker();

    return EXIT_SUCCESS;
}
