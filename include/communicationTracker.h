#ifndef communicationTracker_h
#define communicationTracker_h

#include "../include/types.h"

void getListeDesPairs(struct fileList* filesList, u_short myPort, u_int myId);
void getListeDesPairsFichier(struct fileListEntry* file, u_short myPort, u_int myId);

#endif
