#ifndef peerclient_h
#define peerclient_h

#include "peerfunc.h"
#include "communicationTracker.h"

struct torrentEtPeer {
    struct beerTorrent* torrent;
    struct peerEntry peer;
};

void fillClient(const char* dir);
void startClient() __attribute__((noreturn));
const char *getFilenameExtension(const char* filename) __attribute__ ((pure));
void addTorrent(const char* filename);
u_short buildClientId();

void createTelechargementPourFichier(struct fileListEntry* file);
void* runThreadTelechargementPourFichierEtPeer(void* arg) __attribute__((noreturn)); /**    Paramètre : torrentEtPeer*     **/
void* runThreadPartagePrincipal(void* arg) __attribute__((noreturn)); /**  Paramètre : int*               **/
void* runThreadPartage(void* arg) __attribute__((noreturn)); /**           Paramètre : int*               **/
struct beerTorrent* parseHandshake(int socketDescriptor);
void giveHandshake(int socketDescriptor, struct beerTorrent* torrent);
struct bitfield* getBitField(int socketDescriptor, const struct beerTorrent* torrent);
void sendHeader(u_int length, u_char type, int socketDescriptor);
void giveBitField(const struct bitfield* bitField, int socketDescriptor);
void answerToRequest(struct beerTorrent* torrent, int socketDescriptor, struct bitfield* peerBitField);
void answerToDownloadRequest(u_int length, struct beerTorrent* torrent, int socketDescriptor);
void downloadPiece(u_int pieceIndex, int socketDescriptor, struct beerTorrent* torrent, struct bitfield* peerBitField);
void downloadBlock(u_int pieceIndex, u_int blockOffset, int socketDescriptor, const struct beerTorrent* torrent);
void parsePieceResult(u_int length, int socketDescriptor, struct beerTorrent* torrent);
void registerPieceAsAvailable(u_int pieceIndex, struct beerTorrent* torrent);
struct newPieceListEntry* sendNotificationOfNewPieces(struct beerTorrent* torrent, struct newPieceListEntry* pieceListEntry, int socketDescriptor);
void sendNotificationOfNewPiece(u_int pieceIndex, int socketDescriptor);
void onHaveRequest(u_int length, int socketDescriptor, struct beerTorrent* torrent, struct bitfield* peerBitField);
void downloadAsMuchAsPossiblePieces(struct beerTorrent* torrent, struct bitfield* peerBitField, int socketDescriptor);

#endif
