#define _GNU_SOURCE
#include <sys/types.h>
#include <netinet/in.h> /* struct sockaddr_in */
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <arpa/inet.h>  /* prototypes pour les fonctions dans inet(3N) */
#include <netdb.h>      /* struct hostent */
#include <sys/socket.h> /* constantes, familles... */
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "types.h"

u_int hash(u_char *str) __attribute__ ((pure));
u_int file2hash(int fd);
const char *getFilenameExtension(const char *filename) __attribute__((pure));
struct bitfield* createbitfield(u_int filelength, u_int piecelength);
void deletebitfield(struct bitfield* bf);
void setbitfield(struct bitfield* dst, struct bitfield* src);
void setbitinfield(struct bitfield* bf, u_int id);
bool isinbitfield(struct bitfield* bf, u_int id) __attribute__((pure));
struct beerTorrent* parseTorrent(const char* filename);
int writeSocket(int fd, const u_char* buf, const int len);
int readblock(int fd, u_char* buffer, const int len);
int isDataToRead(int fileDescriptor);
