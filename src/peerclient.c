#include<stdio.h>
#include <errno.h>
#include "../include/peerclient.h"

/* Global Vars*/
struct fileList filesList;
u_int myId;

void startClient()
{
    u_int i;
    pthread_t threadPrincipalDePartage;

    /*Initialisation du socket de partage*/
    int socketDescriptor = socket(PF_INET, SOCK_STREAM, 0);
    socklen_t socketLength;
    struct sockaddr_in socketAddress;
    struct in_addr tempAddr;
    tempAddr.s_addr = INADDR_ANY;
    socketAddress.sin_family = AF_INET;
    socketAddress.sin_port = 0;
    socketAddress.sin_addr = tempAddr;

    if(socketDescriptor == -1)
    {
        perror("Socket creation");
        exit(EXIT_FAILURE);
    }

    if(bind(socketDescriptor, (struct sockaddr*) &socketAddress, sizeof(socketAddress)) == -1)
    {
        perror("Bind creation");
        exit(EXIT_FAILURE);
    }
    if(listen(socketDescriptor, MAXFILES) == -1)
    { /*Note : on prend au plus MAXFILE connections soit une connection par fichier...*/
        perror("Listen creation");
        exit(EXIT_FAILURE);
    }
    socketLength = sizeof(socketAddress);
    if(getsockname(socketDescriptor, (struct sockaddr*) &socketAddress, &socketLength) == -1)
    {
        perror("Getsockname failure");
        exit(EXIT_FAILURE);
    }
    pthread_create(&threadPrincipalDePartage, NULL, runThreadPartagePrincipal, &socketDescriptor);

    while(42)
    {
        getListeDesPairs(&filesList, socketAddress.sin_port, myId);
        for(i = 0; i < filesList.nbfiles; i++)
        {
            createTelechargementPourFichier(filesList.list[i]);
        }

        sleep(REQUESTPERIOD);
    }
}

void addTorrent(const char* filename)
{
    struct fileListEntry* fileEntry = malloc(sizeof(struct fileListEntry));
    fileEntry->torrent = parseTorrent(filename);
    fileEntry->peers = NULL;
    fileEntry->downloadThreads = NULL;

    filesList.list[filesList.nbfiles] = fileEntry;
    filesList.nbfiles++;

    printf("%s de hash %u ajouté\n", filename, fileEntry->torrent->filehash);
}

void fillClient(const char* dir)
{
    char cwd[1024];
    DIR *dp;
    struct dirent *entry;
    struct stat statbuf;

    /* search all beertorrent in dir */
    printf("Remplissage client ...\n");

    if((dp = opendir(dir)) == NULL)
    {
        perror("opendir");
        return;
    }
    if(getcwd(cwd, sizeof(cwd))==NULL)
    {
        perror("Le remplissage du client a foiré");
        exit(EXIT_FAILURE);
    }

    if(chdir(dir)==-1)
    {
        perror("Le remplissage du client a foiré");
        exit(EXIT_FAILURE);
    }
    while((entry = readdir(dp)) != NULL)
    {
        lstat(entry->d_name,&statbuf);
        if(S_ISREG(statbuf.st_mode))
        {
            /* Found a regular file, if ext = .beertorrent, read it */
            if(strcmp(getFilenameExtension(entry->d_name), "beertorrent") == 0)
            {
                addTorrent(entry->d_name);
            }
        }
    }
    closedir(dp);

    if(chdir(cwd)==-1)
    {
        perror("Le remplissage du client a foiré");
        exit(EXIT_FAILURE);
    }
}

u_short buildClientId()
{
    return (u_short) getpid();
}

void createTelechargementPourFichier(struct fileListEntry* file)
{
    struct torrentEtPeer* torrentEtPeer;
    struct threadListEntry* downloadThread;
    u_int i;

    if(file->torrent->numberOfRemaningPieces == 0)
        return;

    for(i = 0; i < file->peers->nbPeers; i++)
    {
        if(file->downloadThreads == NULL)
        {
            torrentEtPeer = malloc(sizeof(struct torrentEtPeer));
            torrentEtPeer->torrent = file->torrent;
            torrentEtPeer->peer = file->peers->pentry[i];
            file->downloadThreads = malloc(sizeof(struct threadListEntry));
            file->downloadThreads->peerId = torrentEtPeer->peer.peerId;
            file->downloadThreads->next = NULL;
            pthread_create(&file->downloadThreads->thread, NULL, runThreadTelechargementPourFichierEtPeer, torrentEtPeer);
        }
        else
        {
            downloadThread = file->downloadThreads;
            while(downloadThread->next != NULL && downloadThread->peerId != file->peers->pentry[i].peerId)
            {
                downloadThread = downloadThread->next;
            }

            if(downloadThread->peerId != file->peers->pentry[i].peerId)
            {
                torrentEtPeer = malloc(sizeof(struct torrentEtPeer));
                torrentEtPeer->torrent = file->torrent;
                torrentEtPeer->peer = file->peers->pentry[i];
                downloadThread->next = malloc(sizeof(struct threadListEntry));
                downloadThread->next->peerId = torrentEtPeer->peer.peerId;
                downloadThread->next->next = NULL;
                pthread_create(&downloadThread->next->thread, NULL, runThreadTelechargementPourFichierEtPeer, torrentEtPeer);
            }
        }
    }
}

void* runThreadTelechargementPourFichierEtPeer(void* arg)
{
    struct torrentEtPeer* input = (struct torrentEtPeer*) arg;
    struct beerTorrent* torrent = input->torrent;
    struct sockaddr_in destination;
    struct bitfield* peerBitField;
    u_int i;
    u_int iterationsDepuisDernierKeepAliveEnvoye = 0;
    u_int iterationsDepuisDernierKeepAliveRecu = 0;

    int socketDescriptor = socket(PF_INET, SOCK_STREAM, 0);

    printf("Téléchargement du fichier %s depuis le peer %u.\n", torrent->filename, input->peer.peerId);

    destination.sin_family = AF_INET;
    destination.sin_addr = input->peer.ipaddr;
    destination.sin_port = input->peer.port;
    if(connect(socketDescriptor, (struct sockaddr*) &destination, sizeof(destination))==-1)
    {
        perror("Echec de connect au peer");
        free(input);
        pthread_exit(EXIT_SUCCESS);
    }
    free(input);

    /*Premier échange*/
    giveHandshake(socketDescriptor, torrent);
    printf("Handshake envoyé\n");
    peerBitField = getBitField(socketDescriptor, torrent);
    printf("Bitfield reçu\n");

    /*On télécharge tout ce que l'on peut*/
    downloadAsMuchAsPossiblePieces(torrent, peerBitField, socketDescriptor);

    while(torrent->numberOfRemaningPieces > 0)
    {
        iterationsDepuisDernierKeepAliveEnvoye++;
        iterationsDepuisDernierKeepAliveRecu++;
        if(iterationsDepuisDernierKeepAliveRecu == TIMEBEFORETIMEOUT)
        {
            printf("Fermeture du téléchargement du fichier %s depuis le peer %u : timeout.\n", torrent->filename, input->peer.peerId);
            close(socketDescriptor);
            pthread_exit(EXIT_SUCCESS);
        }
        if(iterationsDepuisDernierKeepAliveEnvoye == TIMEBEFORETIMEOUT / 2)
        {
            sendHeader(1, KEEP_ALIVE, socketDescriptor);
            iterationsDepuisDernierKeepAliveEnvoye = 0;
        }

        while(isDataToRead(socketDescriptor))
        {
            iterationsDepuisDernierKeepAliveRecu = 0;
            answerToRequest(torrent, socketDescriptor, peerBitField);
        }
        usleep(SLEEPTIME);
    }

    /* On met à jour le compteur de pièces */
    for(i = 0; i < torrent->bf->nbpiece; i++)
    {
        if(isinbitfield(peerBitField, i))
        {
            torrent->compteurDePresence[i]--;
        }
    }

    printf("Fin du téléchargement de %s !\n", torrent->filename);
    deletebitfield(peerBitField);
    close(socketDescriptor);
    pthread_exit(EXIT_SUCCESS);
}

void* runThreadPartagePrincipal(void* arg)
{
    int socketDescriptor = *(int*) arg;
    struct ListeThreadsPartage {
        pthread_t thread;
        int socketDescriptor;
        struct ListeThreadsPartage* next;
    };
    struct ListeThreadsPartage* threadsPartage = NULL;
    struct ListeThreadsPartage* nouveauThread;
    struct sockaddr_in clientSocketAddress;
    socklen_t socketLength = sizeof(clientSocketAddress);

    printf("Thread de partage principal\n");

    while(42)
    {
        nouveauThread = malloc(sizeof(struct ListeThreadsPartage));
        nouveauThread->socketDescriptor = accept(socketDescriptor, (struct sockaddr*) &clientSocketAddress, &socketLength);
        if(nouveauThread->socketDescriptor == -1)
        {
            perror("Connection socket creation");
            exit(EXIT_FAILURE);
        }
        
        pthread_create(&nouveauThread->thread, NULL, runThreadPartage, (void*) &nouveauThread->socketDescriptor);
        nouveauThread->next = threadsPartage;
        threadsPartage = nouveauThread;
    }
}

void* runThreadPartage(void* arg)
{
    int socketDescriptor = *(int*) arg;
    struct beerTorrent* torrent;
    struct newPieceListEntry* lastPieceListEntry;
    u_int iterationsDepuisDernierKeepAliveEnvoye = 0;
    u_int iterationsDepuisDernierKeepAliveRecu = 0;

    printf("Partage de fichier\n");

    torrent = parseHandshake(socketDescriptor);
    printf("Handshake reçu pour le fichier %s\n", torrent->filename);

    lastPieceListEntry = torrent->newPieces;
    pthread_mutex_lock(&torrent->accessMutex);
    while(lastPieceListEntry->next != NULL)
    {
        lastPieceListEntry = lastPieceListEntry->next;
    }
    pthread_mutex_unlock(&torrent->accessMutex);

    giveBitField(torrent->bf, socketDescriptor);
    printf("Bit field envoyé pour le fichier %s\n", torrent->filename);

    while(42)
    {
        iterationsDepuisDernierKeepAliveEnvoye++;
        iterationsDepuisDernierKeepAliveRecu++;
        if(iterationsDepuisDernierKeepAliveRecu == TIMEBEFORETIMEOUT)
        {
            printf("Fermeture du partage du fichier %s : timeout.\n", torrent->filename);
            close(socketDescriptor);
            pthread_exit(EXIT_SUCCESS);
        }
        if(iterationsDepuisDernierKeepAliveEnvoye == TIMEBEFORETIMEOUT / 2)
        {
            sendHeader(1, KEEP_ALIVE, socketDescriptor);
            iterationsDepuisDernierKeepAliveEnvoye = 0;
        }

        while(isDataToRead(socketDescriptor))
        {
            iterationsDepuisDernierKeepAliveRecu = 0;
            answerToRequest(torrent, socketDescriptor, NULL);
        }
        lastPieceListEntry = sendNotificationOfNewPieces(torrent, lastPieceListEntry, socketDescriptor);

        usleep(SLEEPTIME);
    }
}

void answerToRequest(struct beerTorrent* torrent, int socketDescriptor, struct bitfield* peerBitField)
{
    u_int length;
    u_char type;

    if(read(socketDescriptor, &length, sizeof(length)) == -1)
    {
        perror("Echec de read length");
        exit(EXIT_FAILURE);
    }
    if(length == 0) /*Cas des zéros qui peuvent se balader à la fin d'une connection*/
    {
        return;
    }

    if(read(socketDescriptor, &type, sizeof(type)) == -1)
    {
        perror("Echec de read type");
        exit(EXIT_FAILURE);
    }

    printf("Requête de type %x de longueur %u\n", type, length);
    switch(type) {
        case KEEP_ALIVE:
            break;
        case REQUEST:
            answerToDownloadRequest(length, torrent, socketDescriptor);
            break;
        case PIECE:
            parsePieceResult(length, socketDescriptor, torrent);
            break;
        case HAVE:
            onHaveRequest(length, socketDescriptor, torrent, peerBitField);
            break;
        default:
            printf("Requête inconnu de type %x\n", type);
            exit(EXIT_FAILURE);
    }
}

void downloadAsMuchAsPossiblePieces(struct beerTorrent* torrent, struct bitfield* peerBitField, int socketDescriptor)
{
    u_int i;
    u_int pieceChoisie;
    u_int nombreDoccurencePieceChoisie;

    /* On met à jour le compteur de pièces */
    for(i = 0; i < torrent->bf->nbpiece; i++)
    {
        if(isinbitfield(peerBitField, i))
        {
            torrent->compteurDePresence[i]++; /* On se moque de la syncronisation ici : ce compteur n'est qu'indicatif pour le choix des pièces à prendre en priorité*/
        }
    }

    /* Télécharge les pièces disponibles non téléchargés par ordre de priorité */
    while(42)
    {
        pieceChoisie = torrent->bf->nbpiece;
        nombreDoccurencePieceChoisie = UINT_MAX;
        for(i = 0; i < torrent->bf->nbpiece; i++)
        {
            /* Note : une pièce déjà téléchargée est aussi marqué à 1 dans torrent->downloadingPieces. Le test dans ce bitfield n'est ici qu'indicatif (la pièce peut en effet être demandé entre ce test et l'appel de downloadpiece), un vrai test protégé par mutex est fait par downloadPiece */
            if(isinbitfield(peerBitField, i) && !isinbitfield(torrent->downloadingPieces, i) && torrent->compteurDePresence[i] <= nombreDoccurencePieceChoisie)
            {
                pieceChoisie = i;
                nombreDoccurencePieceChoisie = torrent->compteurDePresence[i];
            }
        }
        if(pieceChoisie == torrent->bf->nbpiece)
        {
            return;
        }
        downloadPiece(pieceChoisie, socketDescriptor, torrent, peerBitField);
    }
}

void giveHandshake(int socketDescriptor, struct beerTorrent* torrent)
{
    u_char protocolVersion = 2;

    if(write(socketDescriptor, &protocolVersion, sizeof(protocolVersion)) == -1)
    {
        perror("Echec de write protocolVersion");
        exit(EXIT_FAILURE);
    }
    if(write(socketDescriptor, &torrent->filehash, sizeof(torrent->filehash)) == -1)
    {
        perror("Echec de write fileHash");
        exit(EXIT_FAILURE);
    }
    if(write(socketDescriptor, &myId, sizeof(myId)) == -1)
    {
        perror("Echec de write peerId");
        exit(EXIT_FAILURE);
    }
}

struct beerTorrent* parseHandshake(int socketDescriptor)
{
    u_char protocolVersion;
    u_int fileHash;
    u_int peerId;
    u_int i;
    struct beerTorrent* torrent;

    if(read(socketDescriptor, &protocolVersion, sizeof(protocolVersion)) == -1)
    {
        perror("Echec de read protocolVersion");
        exit(EXIT_FAILURE);
    }
    if(protocolVersion != 2)
    {
        perror("Mauvaise version du protocole");
        exit(EXIT_FAILURE);
    }
    if(read(socketDescriptor, &fileHash, sizeof(fileHash)) == -1)
    {
        perror("Echec de read fileHash");
        exit(EXIT_FAILURE);
    }
    if(read(socketDescriptor, &peerId, sizeof(peerId)) == -1)
    {
        perror("Echec de read peerId");
        exit(EXIT_FAILURE);
    }

    for(i = 0; i < filesList.nbfiles; ++i)
    {
        torrent = filesList.list[i]->torrent;
        if(torrent->filehash == fileHash)
            return torrent;
    }

    printf("Request for an unkown file!\n");
    exit(EXIT_FAILURE);
}

void giveBitField(const struct bitfield* bitField, int socketDescriptor)
{
    sendHeader(1 + bitField->arraysize, BIT_FIELD, socketDescriptor);
    if(writeSocket(socketDescriptor, bitField->array, (int) bitField->arraysize) != (int) bitField->arraysize)
    {
        perror("Echec de write bitField");
        exit(EXIT_FAILURE);
    }
}

struct bitfield* getBitField(int socketDescriptor, const struct beerTorrent* torrent)
{
    u_int length;
    u_char type;
    struct bitfield* bitField = createbitfield(torrent->filelength, torrent->piecelength);

    if(read(socketDescriptor, &length, sizeof(length)) == -1)
    {
        perror("Echec de read length");
        exit(EXIT_FAILURE);
    }

    if(read(socketDescriptor, &type, sizeof(type)) == -1)
    {
        perror("Echec de read type");
        exit(EXIT_FAILURE);
    }
    if(type != BIT_FIELD)
    {
        printf("La requête devrait être de type bitfield et non de type %x\n", type);
        exit(EXIT_FAILURE);
    }

    if(readblock(socketDescriptor, bitField->array, (int) length - 1) != (int) length - 1)
    {
        perror("Echec de read bitField");
        exit(EXIT_FAILURE);
    }

    return bitField;
}

void downloadPiece(u_int pieceIndex, int socketDescriptor, struct beerTorrent* torrent, struct bitfield* peerBitField)
{
    pthread_mutex_lock(&torrent->accessMutex);
    if(isinbitfield(torrent->bf, pieceIndex) || isinbitfield(torrent->downloadingPieces, pieceIndex))
    {
        pthread_mutex_unlock(&torrent->accessMutex);
        return;
    }
    setbitinfield(torrent->downloadingPieces, pieceIndex);
    pthread_mutex_unlock(&torrent->accessMutex);

    printf("Début du téléchargement de la pièce %u\n", pieceIndex);
    while(isDataToRead(socketDescriptor))
    {
        answerToRequest(torrent, socketDescriptor, peerBitField);
    }
    downloadBlock(pieceIndex, 0, socketDescriptor, torrent); /* On ne fait pas de découpage en blocks*/
}

void registerPieceAsAvailable(u_int pieceIndex, struct beerTorrent* torrent)
{
    struct newPieceListEntry* pieceListEntry;
    pthread_mutex_lock(&torrent->accessMutex);

    setbitinfield(torrent->bf, pieceIndex);
    torrent->numberOfRemaningPieces--;

    pieceListEntry = torrent->newPieces;
    while(pieceListEntry->next != NULL)
    {
        pieceListEntry = pieceListEntry->next;
    }
    pieceListEntry->next = malloc(sizeof(struct newPieceListEntry));
    pieceListEntry->next->pieceIndex = pieceIndex;
    pieceListEntry->next->next = NULL;

    pthread_mutex_unlock(&torrent->accessMutex);
    printf("Fin du téléchargement de la pièce %u\n", pieceIndex);
}

struct newPieceListEntry* sendNotificationOfNewPieces(struct beerTorrent* torrent, struct newPieceListEntry* pieceListEntry, int socketDescriptor)
{
    pthread_mutex_lock(&torrent->accessMutex);
    while(pieceListEntry->next != NULL)
    {
        sendNotificationOfNewPiece(pieceListEntry->next->pieceIndex, socketDescriptor);
        pieceListEntry = pieceListEntry->next;
    }
    pthread_mutex_unlock(&torrent->accessMutex);

    return pieceListEntry;
}

void sendNotificationOfNewPiece(u_int pieceIndex, int socketDescriptor)
{
    printf("Notification of new piece %u\n", pieceIndex);

    sendHeader(1, HAVE, socketDescriptor);
    if(write(socketDescriptor, &pieceIndex, sizeof(pieceIndex)) == -1)
    {
        perror("Echec de write pieceIndex");
        exit(EXIT_FAILURE);
    }
}

void onHaveRequest(u_int length, int socketDescriptor, struct beerTorrent* torrent, struct bitfield* peerBitField)
{
    u_int pieceIndex;

    if(length != 1)
    {
        perror("La requête have a une mauvaise longueur\n");
        exit(EXIT_FAILURE);
    }
    if(read(socketDescriptor, &pieceIndex, sizeof(pieceIndex)) == -1)
    {
        perror("Echec de read pieceIndex");
        exit(EXIT_FAILURE);
    }
    if(peerBitField != NULL)
    {
        torrent->compteurDePresence[pieceIndex]++;
        setbitinfield(peerBitField, pieceIndex);
    }

    if(!isinbitfield(torrent->bf, pieceIndex))
    {
        downloadPiece(pieceIndex, socketDescriptor, torrent, peerBitField);
    }
}

void downloadBlock(u_int pieceIndex, u_int blockOffset, int socketDescriptor, const struct beerTorrent* torrent)
{
    u_int blockLength = torrent->piecelength;

    sendHeader(13, REQUEST, socketDescriptor);

    if(write(socketDescriptor, &pieceIndex, sizeof(pieceIndex)) == -1)
    {
        perror("Echec de write pieceIndex");
        exit(EXIT_FAILURE);
    }
    if(write(socketDescriptor, &blockOffset, sizeof(blockOffset)) == -1)
    {
        perror("Echec de write blockOffset");
        exit(EXIT_FAILURE);
    }
    if(write(socketDescriptor, &blockLength, sizeof(blockLength)) == -1)
    {
        perror("Echec de write blockLength");
        exit(EXIT_FAILURE);
    }
}

void parsePieceResult(u_int length, int socketDescriptor, struct beerTorrent* torrent)
{
    u_int pieceIndex;
    u_int blockOffset;
    u_int blockLength;
    u_char* buffer = NULL;

    if(read(socketDescriptor, &pieceIndex, sizeof(pieceIndex)) == -1)
    {
        perror("Echec de read pieceIndex");
        exit(EXIT_FAILURE);
    }
    if(read(socketDescriptor, &blockOffset, sizeof(blockOffset)) == -1)
    {
        perror("Echec de read blockOffset");
        exit(EXIT_FAILURE);
    }
    blockLength = torrent->piecelength;

    buffer = malloc(length - 9);
    if(readblock(socketDescriptor, buffer, (int) length - 9) == -1)
    {
        perror("Echec de read buffer");
        exit(EXIT_FAILURE);
    }
    pthread_mutex_lock(&torrent->fileMutex);
    fseek(torrent->fp, ((long int)torrent->piecelength * (long int)pieceIndex) + (long int)blockOffset, SEEK_SET);
    fwrite(buffer, sizeof(u_char), blockLength, torrent->fp);
    pthread_mutex_unlock(&torrent->fileMutex);
    free(buffer);

    registerPieceAsAvailable(pieceIndex, torrent); /* Comme on a un block par piece, on a ainsi téléchargé la piece*/
}

void answerToDownloadRequest(u_int length, struct beerTorrent* torrent, int socketDescriptor)
{
    u_int pieceIndex;
    u_int blockOffset;
    u_int blockLength;
    u_char* buffer = NULL;

    if(length != 13)
    {
        perror("La requête request a une mauvaise longueur\n");
        exit(EXIT_FAILURE);
    }

    if(read(socketDescriptor, &pieceIndex, sizeof(pieceIndex)) == -1)
    {
        perror("Echec de read pieceIndex");
        exit(EXIT_FAILURE);
    }
    if(read(socketDescriptor, &blockOffset, sizeof(blockOffset)) == -1)
    {
        perror("Echec de read blockOffset");
        exit(EXIT_FAILURE);
    }
    if(read(socketDescriptor, &blockLength, sizeof(blockLength)) == -1)
    {
        perror("Echec de read blockLength");
        exit(EXIT_FAILURE);
    }
    printf("Reçu demande de la pièce %u\n", pieceIndex);

    sendHeader(9 + blockLength, PIECE, socketDescriptor);
    if(write(socketDescriptor, &pieceIndex, sizeof(pieceIndex)) == -1)
    {
        perror("Echec de write pieceIndex");
        exit(EXIT_FAILURE);
    }
    if(write(socketDescriptor, &blockOffset, sizeof(blockOffset)) == -1)
    {
        perror("Echec de write blockOffset");
        exit(EXIT_FAILURE);
    }

    buffer = malloc(sizeof(u_char) * blockLength);
    pthread_mutex_lock(&torrent->fileMutex);
    fseek(torrent->fp, ((long int)torrent->piecelength * (long int)pieceIndex) + (long int)blockOffset, SEEK_SET);
    if(fread(buffer, sizeof(u_char), blockLength, torrent->fp) <= 0)
    {
        perror("Erreur de lecture du fichier");
        exit(EXIT_FAILURE);
    }
    pthread_mutex_unlock(&torrent->fileMutex);
    if(writeSocket(socketDescriptor, buffer, (int) blockLength) == -1)
    {
        perror("Echec de write buffer");
        exit(EXIT_FAILURE);
    }
    free(buffer);
}

void sendHeader(u_int length, u_char type, int socketDescriptor)
{
    if(write(socketDescriptor, &length, sizeof(length)) == -1)
    {
        perror("Echec de write length");
        exit(EXIT_FAILURE);
    }
    if(write(socketDescriptor, &type, sizeof(type)) == -1)
    {
        perror("Echec de write type");
        exit(EXIT_FAILURE);
    }
}

int main(int argc, char* argv[])
{
    const char* directory;
    if(argc > 1)
        directory = argv[1];
    else
        directory = "./beertorrent";

    myId = buildClientId();

    filesList.nbfiles = 0;
    fillClient(directory);

    startClient();

    return EXIT_SUCCESS;
}
