CC=gcc
FLAGBASE= -lpthread -W -Wextra -Wcast-qual -Wcast-align -Wfloat-equal -Wshadow -Wpointer-arith -Wunreachable-code -Wchar-subscripts -Wcomment -Wformat -Werror-implicit-function-declaration -Wmain -Wmissing-braces -Wparentheses -Wsequence-point -Wreturn-type -Wswitch -Wuninitialized -Wundef -Wwrite-strings -Wsign-compare -Wmissing-declarations -pedantic -Wconversion -Wmissing-noreturn -Wall -Wunused -Wsign-conversion -Wunused -Wstrict-aliasing -Wstrict-overflow -Wconversion -Wdisabled-optimization -Wsuggest-attribute=const -Wsuggest-attribute=noreturn -Wsuggest-attribute=pure -Wlogical-op -Wunsafe-loop-optimizations
CFLAGS= -O3 $(FLAGBASE)
EXEC=setup peerclient tracker
CFLAGS99= -std=c99 $(CFLAGS)

all: $(EXEC) 

purge: clean all

debug: CFLAGS = -D DEBUG -g $(FLAGBASE)
debug: all

setup:
	mkdir -p obj

tracker: obj/tracker.o
	gcc -o $@ $^ $(CFLAGS99)

peerclient: obj/peerclient.o obj/peerfunc.o obj/communicationTracker.o
	gcc -o $@ $^ $(CFLAGS)

obj/tracker.o: src/tracker.c include/tracker.h
	$(CC) -c -o $@ $< $(CFLAGS)

obj/peerfunc.o: src/peerfunc.c include/peerfunc.h
	$(CC) -c -o $@ $< $(CFLAGS)

obj/peerclient.o: src/peerclient.c include/peerfunc.h
	$(CC) -c -o $@ $< $(CFLAGS)

obj/communicationTracker.o: src/communicationTracker.c include/communicationTracker.h
	$(CC) -c -o $@ $< $(CFLAGS)
	
clean:
	rm -f obj/*.o
