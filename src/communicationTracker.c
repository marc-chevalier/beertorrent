#include "../include/communicationTracker.h"

void getListeDesPairs(struct fileList* filesList, u_short myPort, u_int myId)
{
    u_int i;

    for(i=0;i<filesList->nbfiles;++i)
    {
        getListeDesPairsFichier(filesList->list[i], myPort, myId);
    }
}

void getListeDesPairsFichier(struct fileListEntry* file, u_short myPort, u_int myId)
{
    int val;
    int ss;  /* socket d'écoute */
    struct clientRequest requete;
    struct peerListEntry* listeDesPairs=malloc(sizeof(struct peerListEntry));
    u_char status;
    u_int i=0;
    struct sockaddr_in information_sur_la_destination;
    u_int peerId;
    struct in_addr ipaddr;
    u_short port;
    printf("Je commence !\n");
    /* socket Internet, de type stream (fiable, bi-directionnel) */
    ss = socket (PF_INET, SOCK_STREAM, 0);

    /* Force la réutilisation de l'adresse si non allouée */
    val = 1;
    setsockopt (ss, SOL_SOCKET, SO_REUSEADDR, &val, sizeof(val));


    information_sur_la_destination.sin_family=AF_INET;
    information_sur_la_destination.sin_addr=file->torrent->trackerip; /* Indiquez l'adresse IP de votre serveur */
    information_sur_la_destination.sin_port=htons(PORT); /* Port écouté du serveur (PORT)*/
    if(connect(ss,(struct sockaddr*)&information_sur_la_destination,sizeof(information_sur_la_destination))==-1)
    {
        perror("Echec de connect au tracker");
        exit(EXIT_FAILURE);
    }

    requete.fileHash=file->torrent->filehash;
    requete.peerId=myId;
    requete.port=myPort;

    printf("* myHash: %x\n", requete.fileHash);
    printf("* myPeerId: %x\n", requete.peerId);
    printf("* myPort: %x\n", requete.port);

    if(write(ss, &requete.fileHash, sizeof(requete.fileHash))==-1)
    {
        perror("Echec de write fileHash");
        exit(EXIT_FAILURE);
    }
    if(write(ss, &requete.peerId, sizeof(requete.peerId))==-1)
    {
        perror("Echec de write peerId");
        exit(EXIT_FAILURE);
    }
    if(write(ss, &requete.port, sizeof(requete.port))==-1)
    {
        perror("Echec de write port");
        exit(EXIT_FAILURE);
    }

    if(read(ss, &status, sizeof(status))==-1)
    {
        perror("Echec de read status");
        exit(EXIT_FAILURE);
    }

    if(status==1)
    {
        fprintf(stderr, "Echec de la réponse du tracker");
        exit(EXIT_FAILURE);
    }

    if(read(ss,&(listeDesPairs->nbPeers), sizeof(listeDesPairs->nbPeers))==-1)
    {
        perror("Echec de read nbPeers");
        exit(EXIT_FAILURE);
    }

    --(listeDesPairs->nbPeers);

    listeDesPairs->pentry = malloc((u_int)(listeDesPairs->nbPeers)*sizeof(struct peerEntry));

    for(i = 0; i < listeDesPairs->nbPeers;)
    {
        if(read(ss, &peerId, sizeof(peerId))==-1)
        {
            perror("Echec de write peerId");
            exit(EXIT_FAILURE);
        }
        if(read(ss, &ipaddr , sizeof(ipaddr))==-1)
        {
            perror("Echec de write ipaddr");
            exit(EXIT_FAILURE);
        }
        if(read(ss, &port, sizeof(port))==-1)
        {
            perror("Echec de read port");
            exit(EXIT_FAILURE);
        }
        if(peerId != myId)
        {
            listeDesPairs->pentry[i].peerId=peerId;
            listeDesPairs->pentry[i].ipaddr=ipaddr;
            listeDesPairs->pentry[i].port=port;
            ++i;

            printf("Other peer %x\n", i);
            printf("* ip: %x\n", ipaddr.s_addr);
            printf("* peerId: %x\n", peerId);
            printf("* port: %x\n", port);
        }
    }

    /*printf("%d\n",myPort);
    printf("%d\n",listeDesPairs->nbPeers);

    for(i=0;i<listeDesPairs->nbPeers;++i)
    {
        printf("%d %d\n", listeDesPairs->pentry[i].peerId, listeDesPairs->pentry[i].port);
    }*/


    file->peers=listeDesPairs;
    printf("Fin !\n");
    close(ss);
}
