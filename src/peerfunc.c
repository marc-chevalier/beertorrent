#include "../include/peerfunc.h"

u_int hash(u_char *str)
{
    u_int hash_ = 0;
    int c;

    while ((c = (int)(*str++)))
        hash_ += (u_int)c;

    return hash_;
}

u_int file2hash(int fd)
{
    int bytes;
    u_char data[1024];
    u_int h = 0;

    while ((bytes = (int)read(fd, data, 1024)) != 0)
        h += hash(data);

    return h;
}

const char *getFilenameExtension(const char *filename)
{
    const char *dot = strrchr(filename, '.');

    if(!dot || dot == filename)
        return "";

    return dot + 1;
}

/* Beertorrent functions */

struct bitfield * createbitfield(u_int filelength, u_int piecelength)
{
    struct bitfield * bf = malloc(sizeof(struct bitfield));

    bf->nbpiece = (filelength-1) / piecelength + 1;
    bf->arraysize = (bf->nbpiece-1) / 8 + 1;
    bf->array = malloc(sizeof(u_char) * bf->arraysize);
    memset(bf->array, 0, bf->arraysize);

    return bf;
}

void deletebitfield(struct bitfield * bf)
{
    free(bf->array);
    free(bf);
}

void setbitfield(struct bitfield * dst, struct bitfield * src)
{
    memcpy(dst->array, src->array, dst->arraysize);
}

void setbitinfield(struct bitfield * bf, u_int id)
{
    bf->array[id/8] |= (u_char)(0x1 << (id%8));
}

bool isinbitfield(struct bitfield * bf, u_int id)
{
    return !!(bf->array[id/8] & (0x1 << (id%8)));
}

struct beerTorrent* parseTorrent(const char* filename)
{

    struct beerTorrent* bt = malloc(sizeof(struct beerTorrent));
    FILE *fp;
    char *line = NULL;
    size_t len = 0;
    size_t ln;
    bt->newPieces = NULL;
    bt->newPieces = malloc(sizeof(struct newPieceListEntry));
    bt->newPieces->pieceIndex = 0;
    bt->newPieces->next = NULL;
    pthread_mutex_init(&bt->accessMutex, NULL);
    pthread_mutex_init(&bt->fileMutex, NULL);

    fp = fopen(filename, "r");
    if (fp == NULL)
    {
        printf("Cannot open %s", filename);
        return NULL;
    }

    /* Format :
     * length (Taille du fichier en octet)
     * hashsum (Unsigned long correspondant à la signature du fichier)
     * name (Nom du fichier)
     * piece length (Taille de chacun des morceaux à télécharger)
     * announce (IP du Tracker)
     * */
    if(getline(&line, &len, fp)==-1)
    {
        printf("Parsage raté ! \n");
        exit(EXIT_FAILURE);
    }
    bt->filelength = (u_int) atol(line);

    if(getline(&line, &len, fp)==-1)
    {
        printf("Parsage raté ! \n");
        exit(EXIT_FAILURE);
    }
    bt->filehash = (u_int) atol(line);

    if(getline(&line, &len, fp)==-1)
    {
        printf("Parsage raté ! \n");
        exit(EXIT_FAILURE);
    }
    ln = strlen(line) - 1;
    if (line[ln] == '\n')
        line[ln] = '\0';
    strcpy(bt->filename, line);

    if(getline(&line, &len, fp)==-1)
    {
        printf("Parsage raté ! \n");
        exit(EXIT_FAILURE);
    }
    bt->piecelength = (u_int) atol(line);

    if(getline(&line, &len, fp)==-1)
    {
        printf("Parsage raté ! \n");
        exit(EXIT_FAILURE);
    }
    ln = strlen(line) - 1;
    if (line[ln] == '\n')
        line[ln] = '\0';
    inet_pton(AF_INET, line, &(bt->trackerip));

    free(line);

    printf("Opening %s\n", bt->filename);

    bt->bf = createbitfield(bt->filelength, bt->piecelength);
    bt->downloadingPieces = createbitfield(bt->filelength, bt->piecelength);

    if( access( bt->filename, F_OK ) != -1 )
    {
        /*file exists - supposely complete*/

        if(!(bt->fp = fopen(bt->filename, "rb")))
        {

            perror("fopen file");
            deletebitfield(bt->bf);
            free(bt);
            return NULL;
        }

        /* set full bitfield*/
        memset(bt->bf->array, 255, bt->bf->arraysize);
        bt->numberOfRemaningPieces = 0;
    }
    else
    {
        /* file doesn't exist*/
        bt->numberOfRemaningPieces = bt->bf->nbpiece;
        if(!(bt->fp = fopen(bt->filename, "w+b")))
        {

            perror("fopen file");
            deletebitfield(bt->bf);
            free(bt);
            return NULL;
        }
        bt->compteurDePresence = malloc(sizeof(u_int) * bt->bf->nbpiece);
    }

    printf("%s added\n", filename);

    return bt;
}

int writeSocket(int fd, const u_char *buf, const int len)
{
    int currentsize=0;
    while(currentsize<len)
    {
        int count=(int)write(fd,buf+currentsize,(size_t)(len-currentsize));
        if(count<0) return -1;
        currentsize+=count;
    }
    return currentsize;
}

int readblock(int fd, u_char* buffer, const int len)
{
    int ret = 0;
    int count = 0;
    while (count < len)
    {
        ret = (int)read(fd, buffer + count, (size_t)(len - count));
        if (ret < 0)
        {
            return -1;
        }
        count += ret;
    }
    return count;
}

int isDataToRead(int fileDescriptor)
{
    fd_set readfs;
    struct timeval waitTime;
    waitTime.tv_sec = 0;
    waitTime.tv_usec = 0;
    FD_ZERO(&readfs);
    FD_SET((long unsigned int)fileDescriptor, &readfs);

    if(select(fileDescriptor + 1, &readfs, NULL, NULL, &waitTime) < 0)
    {
        perror("select");
        exit(EXIT_FAILURE);
    }

    return FD_ISSET((long unsigned int)fileDescriptor, &readfs);
}
